
(defun current-epoch-time-in-seconds ()
  "Returns the epoch time in seconds."
  ;; See below for an explanation of current-time and the high and low values.
  ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Time-of-Day.html
  (let* ((time-values (current-time))
         (high (elt time-values 0))
         (low (elt time-values 1)))
    (+ (* high 65536) low)))

(defun epoch-time-to-string (epoch-time)
  "Converts the given epoch time in seconds or milliseconds into a time-date string."
  (interactive "n\Epoch time (seconds or milliseconds): ")
  (let* ((epoch-time-seconds (if (>= (log epoch-time 10) 12)
                                 (/ epoch-time 1000)
                               epoch-time))
         (high (/ epoch-time-seconds 65536))
         (low (% epoch-time-seconds 65536))
         (time-list (list high low 0 0)))
    (message (format-time-string "%F %T" time-list))))
