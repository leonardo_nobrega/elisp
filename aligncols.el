
(defun aligncols--empty-string-p (string)
  "Empty string predicate."
  (= (length string) 0))

(defun aligncols--split-string (string separator)
  "Like `split-string', but returns an empty list if the given STRING is empty."
  (if (aligncols--empty-string-p string)
      nil
    (split-string string separator)))

(defun aligncols--break-region (region-string)
  "Extract the lines from the given REGION-STRING."
  (aligncols--split-string region-string "\n"))

(defun aligncols--break-line (line-string separator)
  "Tokenizes the LINE-STRING using the SEPARATOR."
  (aligncols--split-string line-string separator))

(defun aligncols--make-lines-list (region-string separator)
  "Makes token lists from the REGION-STRING by first extracting the lines
in the region, then tokenizing each line."
  (mapcar (lambda (line-string) (aligncols--break-line line-string separator))
          (aligncols--break-region region-string)))

(defun aligncols--reduce (f list initial-value)
  "Applies the function F repeatedly to the elements of LIST.
F takes two arguments. The first is the result of the previous application,
the second is one of the LIST's elements.
The first call to F uses INITIAL-VALUE."
  (let ((current initial-value))
    (dolist (element list)
      (setq current (funcall f current element)))
    current))

(defun aligncols--table-dimensions (lines-list)
  "Returns a cons cell containing the number of rows and columns
for the table that will be created from the LINES-LIST."
  (let ((num-rows (length lines-list))
        (num-cols (aligncols--reduce (lambda (current-num-cols tokens-list)
                                       (let ((num-tokens (length tokens-list)))
                                         (cond ((> current-num-cols num-tokens)
                                                current-num-cols)
                                               (t num-tokens))))
                                     lines-list
                                     0)))
    (cons num-rows num-cols)))

(defun aligncols--number-of-rows (table-dimensions)
  "Accessor for the number of rows in TABLE-DIMENSIONS."
  (car table-dimensions))

(defun aligncols--number-of-columns (table-dimensions)
  "Accessor for the number of columns in TABLE-DIMENSIONS."
  (cdr table-dimensions))

(defun aligncols--list-to-vector (list &optional vector-size)
  "Makes a vector with the elements from the given LIST."
  (let* ((size (or vector-size (length list)))
         (vector (make-vector size nil))
         (index 0))
    (dolist (element list vector)
      (aset vector index element)
      (setq index (1+ index)))))

(defun aligncols--convert-to-lines-vector (lines-list table-dimensions)
  "LINES-LIST is a list (lines) of lists (tokens). This function makes a vector
of vectors from it."
  (let ((num-rows (aligncols--number-of-rows table-dimensions))
        (num-cols (aligncols--number-of-columns table-dimensions)))
    (aligncols--list-to-vector
     (mapcar (lambda (tokens-list) (aligncols--list-to-vector tokens-list num-cols))
             lines-list)
     num-rows)))

(defun aligncols--access-cell (lines-vector row column)
  "LINES-VECTOR is a two-dimensional array. This function returns a cell
from the array, given its ROW and COLUMN."
  (aref (aref lines-vector row) column))

(defun aligncols--column-widths (lines-vector table-dimensions)
  "Finds the widths of the columns in the table of strings represented by
LINES-VECTOR."
  (let* ((num-rows (aligncols--number-of-rows table-dimensions))
         (num-cols (aligncols--number-of-columns table-dimensions))
         (widths (make-vector num-cols nil)))
    (dotimes (c num-cols)
      (let ((current-width 0))
        (dotimes (r num-rows)
          (setq current-width
                (max current-width
                     ;; length of the string in the cell (r, c)
                     (length (aligncols--access-cell lines-vector r c)))))
        (aset widths c current-width)))
    widths))

(defun aligncols--pad-string (string length-with-padding &optional alignment)
  "Adds padding to a string.
The allowed values for ALIGNMENT are 'left, 'right and 'center."
  (let* ((string-length (length string))
         (num-padding-chars (max 0 (- length-with-padding string-length)))
         (alignment (or alignment 'right))
         (left-padding (cond
                        ((eq alignment 'left) num-padding-chars)
                        ((eq alignment 'right) 0)
                        ((eq alignment 'center) (/ num-padding-chars 2))))
         (right-padding (cond
                         ((eq alignment 'left) 0)
                         ((eq alignment 'right) num-padding-chars)
                         ((eq alignment 'center) (+ (/ num-padding-chars 2)
                                                    (% num-padding-chars 2))))))
    (concat (make-string left-padding ?\s)
            string
            (make-string right-padding ?\s))))

(defun aligncols--build-table-string (string separator)
  "Makes a table string from the REGION-STRING by tokenizing its lines,
then padding the tokens with spaces to align them vertically."
  (let* ((lines-list (aligncols--make-lines-list string separator))
         (dimensions (aligncols--table-dimensions lines-list))
         (lines-vector (aligncols--convert-to-lines-vector lines-list dimensions)))
    (let ((column-widths (aligncols--column-widths lines-vector dimensions))
          (num-rows (aligncols--number-of-rows dimensions))
          (num-cols (aligncols--number-of-columns dimensions))
          (table-string ""))
      (dotimes (r num-rows)
        (let ((line ""))
          (dotimes (c num-cols)
            (if (> c 0) (setq line (concat line separator)))
            (let* ((cell (aligncols--access-cell lines-vector r c))
                   (width (aref column-widths c))
                   (padded-cell (aligncols--pad-string cell width)))
              (setq line (concat line padded-cell))))
          (setq table-string (concat table-string line "\n"))))
      table-string)))

(defun align-columns (region-start region-end separator)
  (interactive "r\nsSeparator: ")
  (let* ((region-string (buffer-substring region-start region-end))
         (separator (if (aligncols--empty-string-p separator) " " separator))
         (table-string (aligncols--build-table-string region-string separator)))
    (delete-region region-start region-end)
    (insert table-string)))

(defun aligncols--tests ()
  "Unit tests.
Running the tests:
- evaluate this file
- open the *scratch* buffer
- evaluate: (aligncols--tests)
When all tests pass the result will be nil."
  (let ((check (lambda (test-name expected actual)
                 (if (not (equal expected actual))
                     (print (list 'FAIL test-name
                                  'expected expected
                                  'actual actual))))))
    ;; break-region tests
    (let* ((test-string "abc def\ngh ijk")
           (expected '("abc def" "gh ijk"))
           (actual (aligncols--break-region test-string)))
      (funcall check 'break-region expected actual))
    (let* ((test-string "")
           (expected '())
           (actual (aligncols--break-region test-string)))
      (funcall check 'break-region expected actual))

    ;; break-line tests
    (let* ((test-string "abc def gh ijk")
           (separator " ")
           (expected '("abc" "def" "gh" "ijk"))
           (actual (aligncols--break-line test-string separator)))
      (funcall check 'break-line expected actual))
    (let* ((test-string "")
           (separator " ")
           (expected '())
           (actual (aligncols--break-line test-string separator)))
      (funcall check 'break-line expected actual))

    ;; table-dimensions tests
    (let* ((test-string "abc def\ngh ijk")
           (separator " ")
           (expected (cons 2 2))
           (actual (aligncols--table-dimensions (aligncols--make-lines-list test-string separator))))
      (funcall check 'table-dimensions expected actual))
    (let* ((test-string "")
           (separator " ")
           (expected (cons 0 0))
           (actual (aligncols--table-dimensions (aligncols--make-lines-list test-string separator))))
      (funcall check 'table-dimensions expected actual))

    ;; column-widths test
    ;; token widths:  3   2 | 4    2  1| 1 1 2
    (let* ((test-str "abc de\nfghi jk l\nm n op\n")
           (lines-list (aligncols--make-lines-list test-str " "))
           (dimensions (aligncols--table-dimensions lines-list))
           (lines-vector (aligncols--convert-to-lines-vector lines-list dimensions))
           (expected [4 2 2])
           (actual (aligncols--column-widths lines-vector dimensions)))
      (funcall check 'column-widths expected actual))

    ;; pad-string tests
    (let ((expected "abc  ")
          (actual (aligncols--pad-string "abc" 5)))
      (funcall check 'pad-string-right expected actual))
    (let ((expected " abc ")
          (actual (aligncols--pad-string "abc" 5 'center)))
      (funcall check 'pad-string-center expected actual))
    (let ((expected "  abc")
          (actual (aligncols--pad-string "abc" 5 'left)))
      (funcall check 'pad-string-left expected actual))

    ;; build-table-string tests
    (let* ((input-string (concat "abc de f\n"
                                 "g hij\n"
                                 "kl mn opq"))
           (separator " ")
           (expected (concat "abc de  f  \n"
                             "g   hij    \n"
                             "kl  mn  opq\n"))
           (actual (aligncols--build-table-string input-string separator)))
      (funcall check 'build-table-string expected actual))
    ))
